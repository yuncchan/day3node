//Load Express library

var express = require("express");

//Create an instance of the express application

var app = express();

//Configure the routes

app.use(express.static(__dirname + "/public"));
//Remember that the above is the root. Can't access files above or at this level. app.use(express.static(__dirname + "/images"));

app.use("/localPics", express.static(__dirname + "/localPics"));

//Create my server
//Specify the port being used

var port = 3000;

//Bind the app to the port

app.listen (port, function() {
    console.log("Application started on %d port", port);
    }
);
